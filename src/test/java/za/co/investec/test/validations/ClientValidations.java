package za.co.investec.test.validations;

import org.hamcrest.core.Is;
import org.hibernate.validator.internal.engine.ValidatorImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;
import org.springframework.web.context.WebApplicationContext;
import za.co.investec.controller.ClientController;
import za.co.investec.dto.ClientDTO;
import za.co.investec.model.Client;
import za.co.investec.repository.ClientRepo;
import za.co.investec.service.ClientService;
import za.co.investec.service.ClientServiceImpl;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.nio.charset.Charset;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {ClientService.class, ClientController.class})
public class ClientValidations {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientRepo clientRepository;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ClientController controller;

    @Before
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @TestConfiguration
    static class ClientControllerIntegrationContextConfiguration {

        @Bean
        public ClientServiceImpl clientService() {
            return new ClientServiceImpl();
        }
    }

    @Test
    public void invalidClientInput() throws Exception {
        String client = "{\"firstName\":\"\",\"lastName\":\"Thale\",\"mobileNumber\":\"0731236987\",\"idNumber\":\"9212280559080\",\"physicalAddress\": \"Something Soweto\"}";
        mockMvc.perform(MockMvcRequestBuilders.post("/client")
                .content(client)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("firstName", Is.is("First Name is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void validClientInput() throws Exception {
        MediaType textPlainUtf8 = new MediaType(MediaType.TEXT_PLAIN, Charset.forName("UTF-8"));
        String client = "{\"firstName\":\"Lerato\",\"lastName\":\"Thale\",\"mobileNumber\":\"0731236987\",\"idNumber\":\"9212280559080\",\"physicalAddress\": \"Something Soweto\"}";
        mockMvc.perform(MockMvcRequestBuilders.post("/client/")
                .content(client)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(textPlainUtf8));
    }


}
