package za.co.investec.test.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import za.co.investec.dto.ClientDTO;
import za.co.investec.model.Client;
import za.co.investec.repository.ClientRepo;
import za.co.investec.service.ClientService;
import za.co.investec.service.ClientServiceImpl;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {ClientService.class})
public class ClientControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientService clientService;

    @MockBean
    private ClientRepo clientRepository;

    @TestConfiguration
    static class ClientControllerIntegrationContextConfiguration {

        @Bean
        public ClientService clientService() {
            return new ClientServiceImpl();
        }
    }

    @Before
    public void setUp() {
        Client client = new Client((long) 1,"Lerato", "Thale", "0731895097", "9212270669080","Something soweto");

        Mockito.when(clientRepository.findByFirstName(client.getFirstName()))
                .thenReturn(client);
        Mockito.when(clientRepository.findByIdNumber(client.getIdNumber()))
                .thenReturn(client);
    }

    @Test
    public void findByFirstName() {
        String name = "Lerato";
        Client found = clientService.findByFirstName(name);

        assertThat(found.getFirstName(), is(name));
    }


    @Test
    public void saveClient(){
        ClientDTO clientDTO = new ClientDTO((long) 1,"Lerato", "Thale", "0731895097", "9212270669081","Something soweto");

        Client saved = clientService.save(clientDTO);

        assertThat(saved.getIdNumber(), is("9212270669081"));
    }

    @Test
    public void valueExists(){
        String idNumber = "9212270669081";
        Client found = clientService.findByIdNumber(idNumber);

    }
}
