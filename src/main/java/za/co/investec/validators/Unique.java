package za.co.investec.validators;

import za.co.investec.service.ClientService;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueValidator.class)
@Documented
public @interface Unique {
    public String message() default "ID already exists";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<? extends ClientService> service();
    String serviceQualifier() default "";
    String fieldName();
}