package za.co.investec.dto;

import za.co.investec.service.ClientService;
import za.co.investec.validators.Unique;
import za.co.investec.validators.ValidID;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ClientDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "First Name is mandatory")
    @NotNull
    private String firstName;

    @NotBlank(message = "Last Name is mandatory")
    private String lastName;

    @Unique(fieldName = "mobileNumber", message = "Mobile number already exists", service = ClientService.class)
    private String mobileNumber;

    @NotBlank(message = "ID Number is mandatory")
    @ValidID
    @Unique(fieldName = "idNumber", message = "ID number already exists", service = ClientService.class)
    private String idNumber;

    private String physicalAddress;

    public ClientDTO(Long id,String firstName, String lastName, String mobileNumber, String idNumber, String physicalAddress) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.idNumber = idNumber;
        this.physicalAddress = physicalAddress;
    }

    public ClientDTO() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }
}
