package za.co.investec.exception;

public class ClientException extends RuntimeException {

    public ClientException(String value) {
        super("Value exists in DB : " + value);
    }
}
