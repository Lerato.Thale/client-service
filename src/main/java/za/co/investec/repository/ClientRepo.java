package za.co.investec.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.investec.model.Client;

public interface ClientRepo<C> extends CrudRepository<Client, Long> {
    Client findByFirstName(String firstName);

    Client findByIdNumber(String idNumber);
}
