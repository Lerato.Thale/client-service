package za.co.investec.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import za.co.investec.service.ClientService;
import za.co.investec.service.ClientServiceImpl;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueValidator implements ConstraintValidator<Unique, String> {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ClientServiceImpl service;

    private String fieldName;

    @Override
    public void initialize(Unique unique) {
        Class<? extends ClientService> clazz = unique.service();
        this.fieldName = unique.fieldName();
        String serviceQualifier = unique.serviceQualifier();

        if (!serviceQualifier.equals("")) {
            this.service = (ClientServiceImpl) this.applicationContext.getBean(serviceQualifier, clazz);
        } else {
            this.service = (ClientServiceImpl) this.applicationContext.getBean(clazz);
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return !this.service.fieldValueExists(value, this.fieldName);
    }
}
