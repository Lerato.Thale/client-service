package za.co.investec.service;

import org.springframework.transaction.annotation.Transactional;
import za.co.investec.dto.ClientDTO;
import za.co.investec.model.Client;

import java.util.List;

public interface ClientService {

    public Client findByFirstName(String firstName);

    public Client findByIdNumber(String idNumber);

    public Client save(ClientDTO client);

    public List<Client> getAllClients();

    public Client updateClient(ClientDTO client);

    boolean fieldValueExists(Object o, String fieldName);
}
