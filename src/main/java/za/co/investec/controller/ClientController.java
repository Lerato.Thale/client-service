package za.co.investec.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import za.co.investec.dto.ClientDTO;
import za.co.investec.model.Client;
import za.co.investec.service.ClientServiceImpl;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ClientController {

    @Autowired
    ClientServiceImpl clientServiceImpl;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @PutMapping("/client")
    public String updateClient(@RequestBody ClientDTO client) {
         if(clientServiceImpl.updateClient(client) != null){
             return "Client updated";
         }
        return "Could not update Client";
    }

    @PostMapping("/client")
    public String newClient(@Valid @RequestBody ClientDTO client) {

            if (clientServiceImpl.save(client) != null) {
                return "Client Saved";
            }
        return HttpStatus.BAD_REQUEST.toString();
    }

    @GetMapping("/client/{name}")
    Client getClientByName(@RequestParam(value = "name") String name) {
        return clientServiceImpl.findByFirstName(name);
    }
    @GetMapping("/client/")
    List<Client> getAllClients() {
        return (List<Client>) clientServiceImpl.getAllClients();
    }

}
