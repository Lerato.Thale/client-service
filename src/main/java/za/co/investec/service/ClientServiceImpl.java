package za.co.investec.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;
import za.co.investec.dto.ClientDTO;
import za.co.investec.model.Client;
import za.co.investec.repository.ClientRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepo<Client> clientRepo;

    @Transactional
    public Client findByFirstName(String firstName) {
        return clientRepo.findByFirstName(firstName);
    }

    @Transactional
    public Client findByIdNumber(String idNumber) {
        return clientRepo.findByIdNumber(idNumber);
    }

    @Transactional
    public Client save(ClientDTO dto) throws ResponseStatusException{
        Client client = clientToClientDto(dto);
        clientRepo.save(client);
        return client;
    }

    @Transactional
    public List<Client> getAllClients() {
        return (List<Client>) clientRepo.findAll();
    }

    @Transactional
    public Client updateClient(ClientDTO dto) {

        Optional<Client> id = clientRepo.findById(dto.getId());
        Client client = id.get();
        client.setIdNumber(dto.getIdNumber());
        client.setFirstName(dto.getFirstName());
        client.setLastName(dto.getLastName());
        client.setMobileNumber(dto.getMobileNumber());
        client.setPhysicalAddress(dto.getPhysicalAddress());
        Client savedClient = clientRepo.save(client);
        return savedClient;
    }

    private ClientDTO clientDto(Client client) {
        ClientDTO dto = new ClientDTO();
        dto.setId(client.getId());
        dto.setFirstName(client.getFirstName());
        dto.setLastName(client.getLastName());
        dto.setIdNumber(client.getIdNumber());
        dto.setMobileNumber(client.getMobileNumber());
        dto.setPhysicalAddress(client.getPhysicalAddress());
        return dto;
    }

    @Override
    public boolean fieldValueExists(Object value, String fieldName) throws ResponseStatusException {
        Assert.notNull(fieldName);

        if (!(fieldName.equals("idNumber") || fieldName.equals("mobileNumber"))) {
            throw new UnsupportedOperationException("Field name not supported");
        }

        if (value == null) {
            return false;
        }

        return this.clientRepo.findByIdNumber(value.toString()) != null;
    }

    public Client clientToClientDto(ClientDTO clientDTO) {
        Client client = new Client();

        client.setIdNumber(clientDTO.getIdNumber());
        client.setFirstName(clientDTO.getFirstName());
        client.setLastName(clientDTO.getLastName());
        client.setMobileNumber(clientDTO.getMobileNumber());
        client.setPhysicalAddress(clientDTO.getPhysicalAddress());
        return client;
    }
}
