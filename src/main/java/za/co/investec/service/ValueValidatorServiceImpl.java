package za.co.investec.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import za.co.investec.repository.ClientRepo;

@Service
public class ValueValidatorServiceImpl implements FieldValueExists {
    @Autowired
    private ClientRepo clientRepo;

    @Override
    public boolean fieldValueExists(String value, String fieldName) throws UnsupportedOperationException {
        Assert.notNull(fieldName);

        if (!fieldName.equals("idNumber")) {
            throw new UnsupportedOperationException("Field name not supported");
        }

        if(this.clientRepo.findByIdNumber(value) != null) {
            return true;
        }

        return false;
    }
}
